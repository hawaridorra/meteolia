<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getMeteoData($city_name): array
    {
        $response = $this->client->request(
            'GET',
            "https://api.openweathermap.org/data/2.5/weather?q=$city_name&appid=eb3e55ca0093756f2541d5ad27c5021c&units=metric"
        );

        return $response->toArray();
    }
}