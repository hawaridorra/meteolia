<?php

namespace App\Controller;

use App\Form\MeteoType;
use App\Service\CallApiService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MeteoliaController extends AbstractController
{
  /**
   * @Route("/meteolia", name="app_meteolia")
   */
  public function index(CallApiService $callApiService, Request $request): Response
  {
    // form generation
    $form = $this->createForm(MeteoType::class);
    // form validation
    $form->handleRequest($request);
    $city_name = 'lille';
    if ($form->isSubmitted() && $form->isValid()) {
      $data = $form->getData();
      $city_name = $data['city_name'];
    }
    return $this->render('meteolia/index.html.twig', [
      'form' => $form->createView(),
      'data' => $callApiService->getMeteoData($city_name),
    ]);
  }
}